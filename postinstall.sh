ln -s ./Scripts ./App/Scripts
ln -s ./Internals ./App/Internals
patch --forward node_modules/vorpal/dist/vorpal.js < ./App/patches/vorpal.js.patch
patch --forward node_modules/vorpal/dist/vorpal-commons.js < ./App/patches/vorpal-commons.js.patch
