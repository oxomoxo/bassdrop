FROM node:14-alpine

ARG git_username="User Name [user --build-arg git_username= to override]"
ARG git_usermail="user@mail.com [user --build-arg git_usermail= to override]"

WORKDIR /BassDrop

COPY BassDrop* .
COPY package*.json .
COPY postinstall.sh .
COPY App App
COPY Internals Internals

RUN \
	npm config set unsafe-perm true && \
	npm install
RUN \
	apk update && apk upgrade && \
	apk add --no-cache bash git openssh

SHELL ["/bin/bash", "-c"]

EXPOSE 9229:9229
COPY . .
RUN \
	git config user.name "$git_username"; \
	git config user.mail "$git_usermail"

ENV ENV="\$HOME/.ashrc", SHELL='/bin/bash'

RUN \
	echo "ENV=$HOME/.ashrc; export ENV" >> /etc/profile ;\
	echo "alias ll='ls -al'" >> /etc/profile ;\
	echo "if [[ -f /etc/profile.d/locale ]] ; then . /etc/profile.d/locale; fi" >> /etc/profile ;\
	echo "if [[ -f /etc/profile.d/color_prompt ]] ; then . /etc/profile.d/color_prompt; fi" >> /etc/profile ;\
	echo "echo 'Sourced /etc/profile'" >> /etc/profile ;
