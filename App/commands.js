'use strict';

const Vorpal = require('vorpal');

// TODO: Automate the export of BassDrop commands to bash
// 		 via a .bassdrop.basrc file that provides stubs functions

var wrap = require('wrap-ansi');
const { fs } = require('./utils');
const { conf, level, local } = require('./conf');

/**
 * BassDrop is the app entry point it doesn't take any arguments except from process.argv :
 * Usage is as follow : BassDrop [:my:long:level:path [command [arguments...]]]
 * If all arguments are ommited BassDrop will run interactively. If you want to call a command
 * directly from the command line you must first provide a level. Alone, the level string will
 * make BassDrop start interactively but at a specified point in the command tree.
 */
module.exports = function BassDrop()
{
	let level = conf.segtSep;
	let argsv = process.argv;
	if(argsv.length > 2 && argsv[ 2 ][ 0 ] == conf.segtSep)
	{
		level = argsv.splice(2, 1).pop();
	}
	Spin(level, argsv);
};

// Some more specific exports for use by internals
module.exports.Spin = Spin;
module.exports.LevelFromFS = LevelFromFS;
module.exports.BuildCommand = BuildCommand;
module.exports.CommandFromFS = CommandFromFS;

function Spin(_level, _argv)
{
	let cli = InitCLI(_level);

	let locmds = [];
	let local = {};
	if(_level == conf.segtSep)
	{
		local = LocalFromFS(cli, locmds);
		BuildLevel(cli, locmds);
	}

	let incmds = [];
	let inter = InterFromFS(cli, _level, incmds);
	BuildLevel(cli, incmds);

	let lvcmds = [];
	let specs = LevelFromFS(cli, _level, lvcmds);
	BuildLevel(cli, lvcmds);

	cli.catch('catch [all...]')
		.allowUnknownOptions(true)
		.action(CatchAll);

	cli.show();

	if(_argv instanceof Array && _argv.length > 2)
	{
		return cli.parse(_argv);
	}
	else
	{
		PrintSpecs(cli, inter, incmds);
		if(_level == conf.segtSep)
			PrintSpecs(cli, local, locmds);
		PrintSpecs(cli, specs, lvcmds);
	}
}

function CatchAll(_args, _cb)
{
	if(_args.all instanceof Array && _args.all[ 0 ][ 0 ] == conf.segtSep)
	{
		Spin(_args.all[ 0 ]);
		_cb();
	}
}

function InitCLI(_level, _cli = void 0)
{
	var cli = _cli || Vorpal();

	cli.level = _level;

	cli.delimiter(conf.appName + _level + conf.prompt);

	return cli;
}

function Excludes(_specs, _sublist)
{
	if(!(_specs.exclude instanceof Array) || (_specs.exclude.length <= 0))
	{
		return _sublist;
	}

	_specs.exclude = _specs.exclude.map(_str => RegExp(_str));

	return _sublist.filter(_dir =>
	{
		return _specs.exclude.reduce((_res, _xcl) =>
		{
			return _res && _dir.match(_xcl) === null;
		}, true);
	});
}

function LocalFromFS(_cli, _subs)
{
	const l = conf.segtSep;

	if(!fs.isDir(local.root())) return;

	let specs = LoadCommand(l, local.specFile());
	specs.command.action = LoadAction(l, local.actionFile());

	let sublist = fs.listDirectories(local.subDir()).sort();

	sublist = Excludes(specs, sublist);

	for(const sub of sublist)
	{
		let cmd = LoadCommand(l + sub, local.specFile(l + sub));
		cmd.command.action = LoadAction(l + sub, local.actionFile(l + sub));

		_subs.push(cmd);
	}

	return specs;
}

function LevelFromFS(_cli, _level, _subs)
{
	let specs = {
		command: {
			name: level.last(_level),
			help: conf.rArrow,
			level: _level,
			// eslint-disable-next-line no-unused-vars
			action: (_args, _cb) => { Spin(_level); _cb(); }
		}
	};

	if(!fs.isDir(level.root(_level))) return specs;

	specs = CommandFromFS(_level);

	let l = _level;
	if(l[ l.length - 1 ] != conf.segtSep)
	{
		l += conf.segtSep;
	}

	let sublist = fs.listDirectories(level.subDir(_level)).sort();

	sublist = Excludes(specs, sublist);

	for(const sub of sublist)
	{
		_subs.push(CommandFromFS(l + sub));
	}

	return specs;
}

/**
 * CommandFromFS builds a command spec object complete with action (if present) from
 * files located in the folder designed by the level argument. The function expects
 * two files in the [_level] folder :
 * 	* The spec.json file (actual filename can be changed in conf.js) :
 * 	  {
 * 		'prompt':'>',
 * 		'command':{
 * 			'name':'CommandName',
 * 			'help':'Command Help : describe what the command is intended to do',
 * 			'values':['possible','values','for','the','arguments']
 * 		},
 * 		'options':[
 * 			{
 * 				'entry':'-o, --option',
 * 				'help':'Option Help : describe the option's influence on the command',
 * 				'values':['possible','values','for','the','option']
 * 			},
 * 			'--okok',
 * 			'-x'
 * 		]
 * 	  }
 * 	* The action.js file [optional] (actual filename can be changed in conf.js) :
 * 	  The file is a node module exporting a function in the form :
 * 		module.exports = function(_args,_cb)
 * 		{
 * 			// Do something ...
 * 			_cb();
 * 		}
 * 	  It will be used this way : _cli.action(require(level.actionFile(_level)));
 * 	  the returned function can carry an 'autocomplete' member function in the form :
 * 	  function(_input, _iteration, _cb) { ... };
 * @param {String} _level - the levele string is composed of alpha-numeric strings lists separated by colons
 */
function CommandFromFS(_level)
{
	let spec = LoadCommand(_level, level.specFile(_level));
	spec.command.action = LoadAction(_level, level.actionFile(_level));
	return spec;
}

function LoadCommand(_level, _path)
{
	let specs = fs.loadJSON(_path) || {};

	if(!specs.command) specs.command = {
		name: level.last(_level),
		help: conf.rArrow
	};

	specs.level = _level;

	return specs;
}

function LoadAction(_level, _path)
{
	if(fs.isFile(_path)) return require(_path);
	else return (_args, _cb) => { Spin(_level); _cb(); };
}

/*
(_level) => level.root(_level) + conf.pathSep + conf.actionFile,
(_level) => level.root(_level) + conf.pathSep + conf.specFile,
*/

function BuildLevel(_cli, _commands)
{
	if(_commands instanceof Array) for(const command of _commands)
	{
		BuildCommand(_cli, command);
	}
}

function BuildCommand(_cli, _spec)
{
	let cmd = _cli.command(
		_spec.command.display || _spec.command.name,
		_spec.command.help,
		_spec.command.values
	);

	if(_spec.command.display) cmd.alias(_spec.command.name);

	if('values' in _spec.command) cmd.autocomplete(_spec.command.values);
	if('options' in _spec) for(const option of _spec.options)
	{
		BuildOption(cmd, option);
	}

	if(typeof _spec.command.action == 'function')
	{
		cmd.action(_spec.command.action);
		if(typeof _spec.command.action.autocomplete == 'function')
		{
			cmd.autocompletion(_spec.command.action.autocomplete);
		}
	}
	else cmd.action((_args, _cb) => { Spin(_cli.level); _cb(); });
}

/**
 * Internals are stored in a specific folder (see: conf.js) as files which names
 * are litally BassDrop level specifiers [:my:long:leve:*] with an optional end glob
 * @param {Vorpal} _cli - A Vorpal instance
 */
function InterFromFS(_cli, _level, _subs)
{
	let internals = fs.listFiles(level.intern(''));

	// Add default nav stubs to cli for unspecified paths

	let l = _level;
	if(l[ l.length - 1 ] != conf.segtSep)
	{
		l += conf.segtSep;
	}

	let sublist = internals.filter(_str =>
		_str.length > _level.length
		&& !_str.includes(conf.intGlob)
		&& _str.startsWith(_level));

	for(let sub of sublist)
	{
		sub = sub.substring(l.length).split(conf.segtSep)[ 0 ];
		_subs.unshift({
			command: {
				name: sub,
				help: conf.rArrow,
				level: l + sub,
				// eslint-disable-next-line no-unused-vars
				action: (_args, _cb) => { Spin(l + sub); _cb(); }
			}
		});
	}

	// Require internals' code in if needed

	if(internals.includes(_level))
	{
		// Exact match first
		require(level.intern(_level))(_subs, _cli);
	}

	internals = internals
		.filter(_str => _level.length > _str.length - 1 && _str.includes(conf.intGlob))
		.map(_str => _str.substring(0, _str.indexOf(conf.intGlob)))
		.sort((_a, _b) => _a.length < _b.length)
		.filter(_str => _level.startsWith(_str));
	for(let internal of internals)
	{
		require(level.intern(internal + conf.intGlob))(_subs, _cli);
	}

	return { // static data
		command: {
			name: 'Internals',
			help: 'Look in the \'Internals\' folder in the source tree for examples.'
		}
	};
}

/**
 * BuildOption adds an option to a Vorpal command
 * @param {Command} _cmd - A live Vorpal Command instance
 * @param {String[Object} _option it can be
 * 	* String, it will be considered as a Vorpal option specifier :
 * 		'-o [argument], --option [argument]'
 * 	* Object, as comming from the specs.json file :
 * 		{
 * 			'entry':'-o, --option',
 * 			'help':'Help : describe the option's influence on the command',
 * 			'values':['possible','values','for','the','option']
 * 		}
 */
function BuildOption(_cmd, _option)
{
	if(typeof _option == 'string')
	{
		_cmd.option(_option);
	}
	else if(typeof _option == 'object')
	{
		_cmd.option(_option.entry, _option.help, _option.values);
	}
}

function FullCommandName(_cmd)
{
	return (_cmd.display || _cmd.name) + ((_cmd.display) ? '(aka: ' + _cmd.name + ') ' : '');
}

function PrintSpecs(_cli, _specs, _subs)
{
	if(!_cli || !_specs || !_specs.command) return;

	let hlp = _specs.command.help;

	_cli.log('[ ' + FullCommandName(_specs.command) + ' ] ' + hlp);

	if(_specs.command.values instanceof Array && _specs.command.values.length > 0)
	{
		_cli.log('\tPossible values:\n\t\t' + _specs.command.values.join(' '));
	}

	if(_specs.option) for(const option of _specs.option)
	{
		_cli.log('\t - ' + option.entry + ' [' + option.help + ']');
		if(option.values instanceof Array && option.values.length > 0)
		{
			_cli.log('\t   Possible values:\n\t\t' + option.values.join(' '));
		}
	}

	let maxname = _subs.reduce((_max, _sub) => Math.max(_max, FullCommandName(_sub.command).length), 0) + 5; // 5 = 4(tab) + 1
	let maxhelp = process.stdout.columns - maxname;

	for(const sub of _subs)
	{
		let name = '    ' + FullCommandName(sub.command) + ' ';
		let help = wrap(sub.command.help, maxhelp, { hard: true, trim: true, wordWrap: true }).split('\n');

		name += (' ').repeat(maxname - name.length);
		name += help.shift();

		while(help.length) name += '\n' + (' ').repeat(maxname) + help.shift();

		_cli.log(name);
	}
}