'use strict';

const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');
const { conf } = require('./conf');

function listDirectories(_path)
{
	if(!isDir(_path)) return []; // throw('[' + _path + '] is not a directory');
	return fs.readdirSync(_path, { withFileTypes: true })
		.filter(_ent => _ent.isDirectory())
		.map(_ent => _ent.name);
}

function listFiles(_path)
{
	if(!isDir(_path)) return []; // throw('[' + _path + '] is not a directory');
	return fs.readdirSync(_path, { withFileTypes: true })
		.filter(_ent => _ent.isFile())
		.map(_ent => _ent.name);
}

function toDir(_path)
{
	fs.mkdirSync(_path, { recursive: true });
}

function toFile(_path, _str)
{
	toDir(path.dirname(_path));
	fs.writeFileSync(_path, _str, 'utf8');
}

function toJSON(_path, _obj, _tab = '\t', _fn = void 0)
{
	toDir(path.dirname(_path));
	fs.writeFileSync(_path, JSON.stringify(_obj,_fn,_tab), 'utf8');
}

function loadFILE(_path)
{
	if (!isFile(_path)) return void 0; // throw('[' + _path + '] is not a file');
	return fs.readFileSync(_path, 'utf8')
}

function loadTPL(_path,_tpldata)
{
	if (!isFile(_path)) return void 0; // throw('[' + _path + '] is not a file');
	let tpl = fs.readFileSync(_path, 'utf8')
		.split('`')
		.map((_str, _i) => ((_i & 1)?_str.replace('$', '\\$'):_str))
		.join('\\`')
		.replace(/\["::([^:]+)::"\]/g,'\${$1}');
	let nf = Array.concat(Object.keys(_tpldata),['return `' + tpl + '`']);
	return Function.apply(null,nf).apply(null, Object.values(_tpldata));
}

function loadJSON(_path)
{
	if(!isFile(_path)) return void 0; // throw('[' + _path + '] is not a file');
	return require(_path);
}

function isFile(_path)
{
	return fs.existsSync(_path) && fs.lstatSync(_path).isFile();
}

function isDir(_path)
{
	if(_path[ _path.length - 1 ] != conf.pathSep) _path += conf.pathSep; // prevent symlink issues
	return fs.existsSync(_path) && fs.lstatSync(_path).isDirectory();
}

class dtree
{
	constructor(_src = null)
	{
		this.data = _src || {};
	}
	root() { return this.data; }

	get(_where)
	{
		_where = _where.split('.');

		let p = this.data;
		for(let node of _where)
		{
			if(!(node in p))
				p[node] = {};
			p = p[node];
		}
		return p;
	}

	set(_where,_data)
	{
		_where = _where.split('.');
		let t = _where.pop();

		let p = this.data;
		for(let node of _where)
		{
			if(!(node in p))
				p[node] = {};
			p = p[node];
		}

		if (p) p[t] = _data;

		return _data;
	}
}

function twalk(_in, _fn)
{
	if(_in instanceof Array) return _in.map(_in=>twalk(_in,_fn));
	if(typeof _in == 'object' && !(_in instanceof String) && _in !== null)
	{
		let out = {};
		for (const p in _in) out[p] = twalk(_in[p],_fn);
		return out;
	}
	return _fn(_in);
}

function bash(_where, _what)
{
	let ret;
	try {
		ret = execSync( _what, { shell: conf.shell, cwd: _where } );
	} catch (error) {
		return `Error [${ error.code }] : ${ error.message }\nOn calling [${_what}] in [${_where}]`;
	}
	if(Buffer.byteLength(ret) == 0) return "";
	if(ret instanceof Buffer) return ret.toString();
	return ret;
}

// Shorthand for explicit new String('instances')
function S(_str) { return new String(_str); }
// Used below for quoted strings
const YAML = { // The 40LOC YAML stringifier
	stringify(_input, _f, _sep)
	{
		function obj(_input, _tabs, _sep)
		{
			let output = '';
			for (const p in _input)
			{
				output += _tabs + p + ':' + out(_input[ p ], _tabs + _sep, _sep);
			}
			return output;
		}
		function arr(_input, _tabs, _sep)
		{
			let output = '';
			for (const i in _input)
			{
				output += _tabs + '- ' + out(_input[ i ], _tabs + _sep, _sep);
			}
			return output;
		}
		function str(_input, _tabs, _sep)
		{
			return ' "' + _input + '"\n';
		}
		function out(_input, _tabs, _sep, _nl = '\n')
		{
			// if you need a quoted string make it
			// an explicit new String('instance')
			if(_input instanceof String)
			{
				return str(_input, _tabs, _sep);
			}
			if(_input instanceof Array)
			{
				return _nl + arr(_input, _tabs, _sep);
			}
			// Object must be tested last
			// (String and Array are Objects]
			if(_input instanceof Object && _input !== null)
			{
				return _nl + obj(_input, _tabs, _sep);
			}
			// yaml values go unquoted otherwise.
			return ' ' + _input.toString() + '\n';
		}
		return out(_input, '', _sep, '');
	}
};

module.exports = {
	fs: {
		listDirectories: listDirectories,
		listFiles: listFiles,
		loadTPL: loadTPL,
		loadJSON: loadJSON,
		loadFILE: loadFILE,
		toFile: toFile,
		isFile: isFile,
		toJSON: toJSON,
		toDir: toDir,
		isDir: isDir
	},
	bash: bash,
	YAML: YAML,
	S:S
};
