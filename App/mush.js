'using strict';

// TODO: Document this potent piece of code proprely so it can be used
// TODO: Do so in a dedicated MarkDown file.

const { fs, bash, S, YAML } = require('./utils');

let renderers =
{	// all renders result in a string
	raw : (_in) => _in,
	join: (_in) => (_in instanceof Array) ? _in.join('\n') : '',
	json: (_in) => JSON.stringify(_in, void 0, '\t'),
	yaml: (_in) => YAML.stringify(_in, void 0, '  '),
	bash: function (_in) { bash(this.conf.target, _in);},
	tmpl: function (_in) {
		return fs.loadTPL(this.root + _in, { _conf: this.conf });
	},
	module: (_in) => 'module.exports = ' + JSON.stringify(_in, (_key, _val) => (typeof _val == 'function') ? _val.toString() : _val, '\t')
};

let callchan = (_mush, _tname, _target, _channel, _args) =>
{
	if (!_target) return;

	if (typeof _target['PRE:' +_channel] == 'function')
	{
		console.log('Target: ', _tname,'\t'+'PRE:' + _channel);
		_target['PRE:' +_channel].apply(_mush, _args);
	}
	if (typeof _target[        _channel] == 'function')
	{
		console.log('Target: ', _tname,'\t'+_channel);
		_target[        _channel].apply(_mush, _args);
	}
	if (typeof _target['POST:'+_channel] == 'function')
	{
		console.log('Target: ', _tname,'\t'+'POST:' + _channel);
		_target['POST:'+_channel].apply(_mush, _args);
	}
}

let wholechan = (_mush, _targets,_channel, ..._args) =>
{
	for (let target in _targets)
	{
		callchan(_mush, target, _targets[target], _channel, _args);
	}
}

function twalk(_in, _fn)
{
	if(_in instanceof Array) return _in.map(_in=>twalk(_in,_fn));
	if (_in && typeof _in == 'object'
		&& !(_in instanceof String)
		&& !(_in instanceof Number)
	){
		let out = {};
		for (const p in _in) out[p] = twalk(_in[p],_fn);
		return out;
	}
	return _fn(_in);
}

class mush
{
	constructor(_root = null, _grafts = null,_channels = null,_conf = null)
	{
		this.root = (_root || '.') + '/render';
		this.grafts = _grafts || [];
		this.conf = _conf || {};
		this.channels = {};
		if (_channels instanceof Array) for(let channel of _channels)
		{
			this.channel(channel.name, channel.file, channel.type, channel.args)
		}
	}

	graft(_name)
	{
		this.grafts.push(_name);
	}

	channel(_name,_file,_type,_args)
	{
		if (!Object.keys(renderers).includes(_type)) return null;
		this.channels[_file] = {
			name: _name,
			file: _file,
			type: _type || 'raw',
			args: _args || [],
			transform: []
		};
		return this;
	}

	build(_kind,_args)
	{
		this.targets = {
			common : require(this.root + '/Common'),
			[_kind] : require(this.root + '/' + _kind)
		};

		for (let graft of this.grafts)
		{
			this.targets[graft] = require(this.root + '/' + graft);
		}

		wholechan(this,this.targets, 'PARSE:ARGS', _args, this.conf);

		for (let channel in this.channels)
		{
			if (!Object.prototype.hasOwnProperty.call(this.channels,channel)) continue;

			channel = this.channels[channel];
			wholechan(this,this.targets, channel.name, this.conf, channel, ...channel.args);
		}

		wholechan(this,this.targets, 'BUILD:END', this.conf, this.channels);

		return this;
	}

	mush()
	{
		fs.toDir(this.conf.target);

		let run = { version: mush.version, root: this.root, conf: this.conf, channels: this.channels, tmpl: {} };

		let inrun = (_in) => (typeof _in == 'function') ? _in(this.conf) : _in;
		let insto = (_in) => (typeof _in == 'function') ? _in.toString() : _in;

		for (let file in this.channels)
		{
			let out = this.channels[file];
			if (!out.content) continue;
			if (out.type == 'tmpl')
			{
				run.tmpl[out.content] = fs.loadFILE(this.root + out.content);
			}
			let content = twalk(out.content,inrun);
			out.content = twalk(out.content,insto);
			content = renderers[out.type].call(this, content);
			if (out.transform instanceof Array)
			{
				out.xform = [];
				for (let xform of out.transform)
				{
					if (typeof xform == 'function')
						content = xform.call(this, this.conf, this.channels, content);
					out.xform.push(xform.toString());
				}
				delete out.transform;
			}
			console.log('Mushing: ', file);
			fs.toFile(this.conf.target + file, content);
		}

		fs.toJSON(this.conf.target + '/mush-run.json', run);

		wholechan(this,this.targets, 'EPILOGUE', this.conf, this.channels);

		return run;
	};
}

mush.S = S;
mush.YAML = YAML;
mush.version = "0.2.0";

module.exports = mush;
