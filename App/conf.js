'use strict';

/* eslint-disable no-undef */

const path = require('path');

const conf = {
	appName: path.basename(path.dirname(__dirname)),// The name of the app is 'BassDrop', but it will take the name of the folder she lives in
	appRoot: path.resolve(__dirname),

	admin: ' ! ',
	prompt: '] ',
	pathSep: '/',
	segtSep: ':',
	intGlob: '*',
	rArrow: '⇒',

	shell: process.env['SHELL'] || '/bin/sh',

	subDir: 'Scripts',
	intern: 'Internals',
	specFile: 'specs.json',
	actionFile: 'action.js',
	local: '.' + path.basename(path.dirname(__dirname)).toLowerCase()
};

const dirs = {
	// This conf is for the special case of the .bassdrop folder in PWD
	root: // returns the actual folder's path given a BassDrop level
		(_level) => ((_level.length <= 1) ? '' : _level.replace(RegExp(conf.segtSep, 'g'), conf.pathSep + conf.subDir + conf.pathSep)),
	subDir: // returns the folder where sub scripts are living given a BassDrop level
		(_level) => dirs.root(_level) + conf.pathSep + conf.subDir + conf.pathSep, // last slash needed if Script target dir is a symlink
	actionFile: // returns the local command action file given a BassDrop level
		(_level) => dirs.root(_level) + conf.pathSep + conf.actionFile,
	specFile: // returns the local specification file given a BassDrop level
		(_level) => dirs.root(_level) + conf.pathSep + conf.specFile
};

const level = {
	// BassDrop is file based, these routines enforce a directoory structure
	// thus are considered as configuration (no file should be accessed directly)
	root: // returns the actual folder's path given a BassDrop level
		(_level) => conf.appRoot + dirs.root(_level),
	subDir: // returns the folder where sub scripts are living given a BassDrop level
		(_level) => conf.appRoot + dirs.subDir(_level),
	actionFile: // returns the local command action file given a BassDrop level
		(_level) => conf.appRoot + dirs.actionFile(_level),
	specFile: // returns the local specification file given a BassDrop level
		(_level) => conf.appRoot + dirs.specFile(_level),

	intern: // returns the folder where system scripts are living
		(_level) => conf.appRoot + conf.pathSep + conf.intern + conf.pathSep + _level,

	parent: // works ~ like path.dirname
		(_level) => _level.substring(0, _level.lastIndexOf(conf.segtSep)) || conf.segtSep,
	last: // works ~ like path.basename
		(_level) => _level.substring(_level.lastIndexOf(conf.segtSep) + 1),
};

const local = {
	// This conf is for the special case of the .bassdrop folder in PWD
	folder: // returns the .bassdrop folder
		(_root) => _root + conf.pathSep + conf.local,
	root: // returns the folder BassDrop was called into
		(_level) => local.folder(process.env[ 'PWD' ]) + dirs.root(_level || conf.segtSep),
	subDir: // returns the folder where sub scripts are living given a BassDrop level
		(_level) => local.folder(process.env[ 'PWD' ]) + dirs.subDir(_level || conf.segtSep),
	actionFile: // returns the local command action file given a BassDrop level
		(_level) => local.folder(process.env[ 'PWD' ]) + dirs.actionFile(_level || conf.segtSep),
	specFile: // returns the local specification file given a BassDrop level
		(_level) => local.folder(process.env[ 'PWD' ]) + dirs.specFile(_level || conf.segtSep)
};

module.exports = {
	conf: conf,
	level: level,
	local: local
};

